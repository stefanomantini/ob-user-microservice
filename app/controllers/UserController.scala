package controllers

import javax.inject._

import models.User
import utils.EmailValidator
import play.api.Logger
import play.api.libs.json._
import play.api.mvc._
import play.modules.reactivemongo._
import reactivemongo.api.ReadPreference
import reactivemongo.play.json._
import reactivemongo.play.json.collection._
import reactivemongo.bson.BSONDocument

import utils.Errors

import scala.concurrent.{ExecutionContext, Future}


/**
  * Controller directly stores and retrieves [models.User] instances into a MongoDB Collection
  */
@Singleton
class UserController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext) extends Controller with MongoController with ReactiveMongoComponents {

  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("user"))

  /**
   * Create a new user in the DB
   * @Params user object JSON
   * @Return success/error
   */
  def create = Action.async(parse.json) {
    Logger.debug(s"recieved: "+parse.json)

    request => Json.fromJson[User](request.body) match {
      case JsSuccess(user, _) =>

      //does the user email exist in the db?
      val futureUsersList: Future[List[User]] = usersFuture.flatMap {
        _.find(Json.obj("email" -> user.email)).
        cursor[User](ReadPreference.primary).
        collect[List]()
      }

        futureUsersList.map { users =>
        //if there's already a user with this email reject update
        if(users.size > 0){
          Logger.debug(s"users with this email already exist: "+ user.email + " " + users.size)
          BadRequest("email already registered")
        }
      }

        for {
          users <- usersFuture
          lastError <- users.insert(user)
        } yield {
          Logger.debug(s"Successfully inserted with LastError: $lastError"+user)
          Created("Created 1 user: " + user)
        }
      case JsError(errors) =>
        //db error handles required fields for now..
        Future.successful(BadRequest("Invalid request, all fields are required"))
    }
  }

  /**
  * TODO implement method to delete by email
  * @Params email to be deleted
  * @Return error/success
  */



  /**
  * Finds all records in the user collection
  * @Return All instances of a user
  */
  def findAll() = Action.async{
    val futureUsersList: Future[List[User]] = usersFuture.flatMap {
      _.find(Json.obj()).cursor[User](ReadPreference.primary).collect[List]()
    }
    futureUsersList.map { users =>
      Ok(Json.toJson(users))
    }
  }

  /**
  * Finds all instances of a user in the DB (should be unique)
  * @Params Email to be found
  * @Return All instances of a user
  */
  def findByEmail(email: String) = Action.async {
    // let's do our query
    val futureUsersList: Future[List[User]] = usersFuture.flatMap {
      // find all cities with email `name`
      _.find(Json.obj("email" -> email)).
      // perform the query and get a cursor of JsObject
      cursor[User](ReadPreference.primary).
      // Coollect the results as a list
      collect[List]()
    }
    // everything's ok! Let's reply with a JsValue
    futureUsersList.map { users =>
      Ok(Json.toJson(users))
    }
  }

}
